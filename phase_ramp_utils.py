#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Wed Nov  4 16:22:54 2015

@author: jdasilva
"""
# standard packages
import time

# third party packages
import matplotlib.gridspec as gridspec
import matplotlib.pyplot as plt
from matplotlib.widgets import MultiCursor, Button#, RectangleSelector
from matplotlib.widgets import TextBox
import numpy as np
from scipy.fftpack import fftfreq
from skimage.restoration import unwrap_phase

# local packages
from io_utils import LoadData, SaveData
from register_translation_fast import register_translation
from roipoly import roipoly

__all__=[
        u'rmphaseramp',
        u'remove_linearphase',
        u'gui_plot',
        u'IndexTracker']

def rmphaseramp(a, weight=None, return_phaseramp=False):
    """
    Attempts to remove the phase ramp in a two-dimensional complex array
    ``a``.

    Parameters
    ----------
    a : ndarray
        Input image as complex 2D-array.

    weight : ndarray, str, optional
        Pass weighting array or use ``'abs'`` for a modulus-weighted
        phaseramp and ``Non`` for no weights.

    return_phaseramp : bool, optional
        Use True to get also the phaseramp array ``p``.

    Returns
    -------
    out : ndarray
        Modified 2D-array, ``out=a*p``
    p : ndarray, optional
        Phaseramp if ``return_phaseramp = True``, otherwise omitted

    Examples
    --------
    >>> b = rmphaseramp(image)
    >>> b, p = rmphaseramp(image , return_phaseramp=True)
    """

    useweight = True
    if weight is None:
        useweight = False
    elif weight == 'abs':
        weight = np.abs(a)

    ph = np.exp(1j*np.angle(a))
    [gx, gy] = np.gradient(ph)
    gx = -np.real(1j*gx/ph)
    gy = -np.real(1j*gy/ph)

    if useweight:
        nrm = weight.sum()
        agx = (gx*weight).sum() / nrm
        agy = (gy*weight).sum() / nrm
    else:
        agx = gx.mean()
        agy = gy.mean()

    (xx, yy) = np.indices(a.shape)
    p = np.exp(-1j*(agx*xx + agy*yy))

    if return_phaseramp:
        return a*p, p
    else:
        return a*p

def remove_linearphase(image,mask):

    ph = np.exp(1j*np.angle(image))
    [gx, gy] = np.gradient(ph)
    gx = -np.real(1j*gx/ph)
    gy = -np.real(1j*gy/ph)

    nrm = mask.sum()
    agx = (gx*mask).sum() / nrm
    agy = (gy*mask).sum() / nrm

    (xx, yy) = np.indices(image.shape)
    p = np.exp(-1j*(agx*xx + agy*yy)) # ramp
    ph_corr = ph*p # correcting ramp
    ph_corr *= np.conj((ph_corr*mask).sum() / nrm) # taking the mask into account

    # applying to the image
    im_output = np.abs(image)*ph_corr
    ph_err = (mask*np.angle(ph_corr)**2).sum() / nrm

    return im_output#, ph_err
        
def remove_linearphase_old(image,mask,upsamp):
    """
    Removes linear phase from object considering only pixels where mask is
    unity, arrays have center on center of array
    Inputs:
        image  = Image
        mask   = Binary array with ones where the linear phase should be
                  computed from
        upsamp = Linear phase will be removed within 2*pi/upsamp peak to valley
                  in radians    
    @author: Julio Cesar da Silva (e-mail:jdasilva@esrf.fr)

    Inspired by remove_linearphase.m created by Manuel Guizar-Sicairos in Aug 19th, 2010.
    Please, cite: Manuel Guizar-Sicairos, Ana Diaz, Mirko Holler, Miriam S. Lucas, 
    Andreas Menzel, Roger A. Wepf, and Oliver Bunk, "Phase tomography from x-ray 
    coherent diffractive imaging projections," Opt. Express 19, 21345-21357 (2011)
    """
    p0 = time.time()
    shift, error, diffphase = register_translation(np.fft.ifftshift(mask*np.abs(image)),np.fft.ifftshift(mask*image),upsamp)
    #shift, error, diffphase = register_translation(mask*np.abs(image),mask*image,upsamp)
    if shift[0]!=0 or shift[1]!=0:
        nr,nc = image.shape
        ar = np.arange(-np.floor(nr/2),np.ceil(nr/2))
        ac = np.arange(-np.floor(nc/2),np.ceil(nc/2))
        #~ Nr,Nc = fftfreq(nr),fftfreq(nc)
        #Nr,Nc = np.fft.ifftshift(fftfreq(nr)),np.fft.ifftshift(fftfreq(nc))
        #~ Nc,Nr = np.meshgrid(Nc,Nr)
        Nc,Nr = np.meshgrid(ac,ar) # FFT frequencies
        image*=np.exp(1j*2*np.pi*(-shift[0]*Nr/nr-shift[1]*Nc/nc))
        
    image*=np.exp(1j*diffphase)
    print("shifts: [{} , {}]".format(shift[0],shift[1]))
    print("Phase difference: {}".format(diffphase))
    print('Time elapsed: {} s'.format(time.time()-p0))
    return image#*np.exp(1j*diffphase)

def gui_plot(stack_objs,**inputparams):
    plt.close('all')
    fig = plt.figure(4)
    gs = gridspec.GridSpec(3, 3, #figure=4,
                        width_ratios=[8, 3, 2],
                        height_ratios=[8, 4.5, 0.5]
                        )
    ax1 = plt.subplot(gs[0])
    ax2 = plt.subplot(gs[3])
    tracker = IndexTracker(
              ax1,ax2,stack_objs,**inputparams)
              #inputparams['pathfilename'],inputparams['pathfilename'])
    #### Button draw mask
    axdraw = plt.axes([0.58,0.82,0.19,0.06])
    bdraw = Button(axdraw,'draw mask')
    bdraw.on_clicked(tracker.draw_mask)
    #### Button Close figure
    axclose = plt.axes([0.78,0.82,0.19,0.06])
    bclose = Button(axclose,'close figure')
    bclose.on_clicked(tracker.onclose)
    #### Button add mask
    axadd = plt.axes([0.58,0.72,0.19,0.06])
    badd = Button(axadd,'add mask')
    badd.on_clicked(tracker.add_mask)
    #### Button Apply mask
    axapply = plt.axes([0.78,0.72,0.19,0.06])
    bapply = Button(axapply,'apply mask')
    bapply.on_clicked(tracker.apply_mask)
    #### Button mask to all
    axmaskall = plt.axes([0.58,0.62,0.19,0.06])
    bmaskall = Button(axmaskall,'mask all')
    bmaskall.on_clicked(tracker.mask_all)
    #### Button Apply all mask
    axapplyall = plt.axes([0.78,0.62,0.19,0.06])
    bapplyall = Button(axapplyall,'apply all masks')
    bapplyall.on_clicked(tracker.apply_all_masks)
    #### Button remove mask
    axremove = plt.axes([0.58,0.52,0.19,0.06])
    bremove = Button(axremove,'remove mask')
    bremove.on_clicked(tracker.remove_mask)
    #### Button remove phase ramp
    axrmramp = plt.axes([0.78,0.52,0.19,0.06])
    brmramp = Button(axrmramp,'remove ramp')
    brmramp.on_clicked(tracker.remove_ramp)
    #### Button remove all mask
    axremoveall = plt.axes([0.58,0.42,0.19,0.06])
    bremoveall = Button(axremoveall,'remove all mask')
    bremoveall.on_clicked(tracker.remove_all_mask)
    #### Button remove all phase ramp
    axrmrampall = plt.axes([0.78,0.42,0.19,0.06])
    brmrampall = Button(axrmrampall,'remove all ramp')
    brmrampall.on_clicked(tracker.remove_rampall)
    #### Button Save masks
    axsave = plt.axes([0.58,0.32,0.19,0.06])
    bsave = Button(axsave,'save masks')
    bsave.on_clicked(tracker.save_masks)
    #### Button unwrap
    axunwrap = plt.axes([0.78,0.32,0.19,0.06])
    bunwrap = Button(axunwrap,'unwrap')
    bunwrap.on_clicked(tracker.unwrapping_phase)
    #### Button load mask from file
    axload = plt.axes([0.58,0.22,0.19,0.06])
    bload = Button(axload,'load masks')
    bload.on_clicked(tracker.load_masks)
    #### Button unwrap all
    axunwrapall = plt.axes([0.78,0.22,0.19,0.06])
    bunwrapall = Button(axunwrapall,'unwrap all')
    bunwrapall.on_clicked(tracker.unwrapping_all)

    #### text boxes
    axboxprojn = plt.axes([0.125, 0.05, 0.1, 0.06])
    text_box = TextBox(axboxprojn, 'Goto #', initial="1")
    text_box.on_submit(tracker.submit)
    #~ text_box.on_text_change(tracker.updateval)

    #### Colormap boxes
    axboxvmin = plt.axes([0.67,0.05,0.1,0.06])
    textboxvmin = TextBox(axboxvmin, 'vmin', initial="None")
    textboxvmin.on_submit(tracker.cmvmin)
    axboxvmax = plt.axes([0.87,0.05,0.1,0.06])
    textboxvmax = TextBox(axboxvmax, 'vmax', initial="None")
    textboxvmax.on_submit(tracker.cmvmax)

    # give the name for colormap boxes
    cmap_title = plt.axes([0.72,0.14,0.1,0.06])
    cmap_title.set_axis_off()
    cmap_title.text(0,0, 'Colormap',fontsize = 14)
    #axboxvmin.set_title('Colormap')

    #### Buttons Prev/Next
    axprev = plt.axes([0.28,0.05,0.05,0.06])
    axnext = plt.axes([0.35,0.05,0.05,0.06])
    bnext = Button(axnext,'>')
    bnext.on_clicked(tracker.up)
    bprev = Button(axprev,'<')
    bprev.on_clicked(tracker.down)
    #### Button Play
    axplay = plt.axes([0.445,0.05,0.1,0.06])
    bplay = Button(axplay,'play')
    bplay.on_clicked(tracker.play)
    #### Connect scroll and key_press events
    fig.canvas.mpl_connect('scroll_event', tracker.onscroll)
    fig.canvas.mpl_connect('key_press_event', tracker.key_event)
    multi = MultiCursor(fig.canvas, (ax1, ax2), color='r', lw=1)
    plt.show(block=False)
    a = input('Press Enter to finish\n')
    # output of the linear phase removal
    stack_phasecorr=tracker.X1.copy() #tracker.X1 is where the data is stored
    plt.close('all')
    return stack_phasecorr
    
class IndexTracker(object):
    def __init__(self, ax1, ax2, X1, **params):#pathfilename,samplename):
        #self.fig = plt.figure(4)
        self.ax1 = ax1
        self.ax2 = ax2
        ax1.set_title('Use scroll wheel or \n left/right arrows to navigate images')

        self.X1 = X1.copy()
        if np.iscomplexobj(self.X1):
            raise ValueError('The array is complex and must be only phase')
            
        #self.X2 = X2
        self.X2=self.X1[:,np.int(X1.shape[1]/2.),:].copy()
        self.mask = np.zeros_like(X1,dtype=np.bool)
        self.slices,rows, cols = X1.shape
        self.ind = 0
        self.params = params
        #self.mask_ind = []
        #self.pathfilename = params['pathfilename'] # to be fixed
        #self.samplename = params['samplename']

        self.im1 = self.ax1.imshow(self.X1[self.ind, :, :],cmap='bone', vmin = -np.pi/2, vmax = np.pi/2)
        self.vmin,self.vmax = self.im1.get_clim() # get colormap limits
        self.ax1.axis('tight')
        self.im2, = self.ax2.plot(self.X2[self.ind, :])
        self.ax2.plot([0,self.X1.shape[2]],[0,0])
        self.pmin,self.pmax = self.ax2.get_ylim() # get plot limits
        #ax2.axis('tight')
        #self.ax2.set_ylim([-np.pi/2,np.pi/2])
        self.ax2.set_ylim([-np.pi,np.pi])
        self.ax2.set_xlim([0,cols])
        self.update()
        print('Done. When finished, close figure and press <Enter> to exit')
    # set the vmin and vmax on colormap
    def cmvmin(self,val):
        if eval(val)>= self.vmax:
            print('vmin is equal or larger than vmax. Choose smaller value')
        else:
            self.vmin = eval(val) #np.clip(eval(val), 0, self.vmax - 1)
            self.pmin = self.vmin
            self.update()

    def cmvmax(self,val):
        if eval(val)<= self.vmin:
            print('vmax is equal or smaller than vmin. Choose larger value')
        else:
            self.vmax = eval(val)
            self.pmax = self.vmax
            self.update()
        
    # move projection number up/down using the mouse scroll wheel
    def onscroll(self, event):
        if event.button == 'up':
            if self.ind==self.slices:#X1.shape[0]:
                print('You reached the maximum number of projections')
            else:
                self.ind = np.clip(self.ind + 1, 0, self.slices - 1)
                print("{} {} - projection {}".format(event.button, event.step,self.ind+1))
        else:
            if self.ind==0:
                print('You reached the first projection')
            else:
                self.ind = np.clip(self.ind - 1, 0, self.slices - 1)
                print("{} {} - projection {}".format(event.button, event.step,self.ind+1))
        self.update()
    # move projection number up/down using right/left arrows in the keyboard
    def key_event(self, event):
        if event.key == 'right':
            if self.ind==self.X1.shape[0]:
                print('You reached the maximum number of projections')
            else:
                self.ind = np.clip(self.ind + 1, 0, self.slices - 1)
                print("{} - projection {}".format(event.key,self.ind+1))
        elif event.key == 'left':
            if self.ind==0:
                print('You reached the first projection')
            else:
                self.ind = np.clip(self.ind - 1, 0, self.slices - 1)
                print("{} - projection {}".format(event.key,self.ind+1))
        else:
            return
        self.update()
    
    # Draw the mask using roipoly.py    
    def draw_mask(self,event):
        #if self.mask_shape == 'p':
        print('\nDrawing the poly')
        self.img_mask = self.X1[self.ind, :, :]+self.mask[self.ind,:,:]
        fig_mask = plt.figure()
        ax_mask = fig_mask.add_subplot(111)
        ax_mask.imshow(self.img_mask,cmap='bone')
        self.ROI_draw = roipoly(ax=ax_mask)
        #self.update()
        #self.ROI_draw = roipoly(ax=self.ax1)
        
        #self.mask_ind.append(self.ind)
        #self.mask_ind = self.ind.copy()
    
    # add the mask to the plot    
    def add_mask(self,event):
        print('\nAdding mask')
        #self.mask[self.mask_ind,:,:] |= self.ROI_draw.getMask(self.img_mask)
        self.mask[self.ind,:,:] |= self.ROI_draw.getMask(self.img_mask)
        #if len(self.mask_ind)==1:
        #    self.mask[self.mask_ind[0],:,:] |= self.ROI_draw.getMask(self.img_mask)
        #else:
        #    for ii in self.mask_ind:
        #       self.mask[ii,:,:] |= self.ROI_draw.getMask(self.img_mask) 
        self.ROI_draw.displayROI()
        self.update()
    
    # use the same mask for all projections        
    def mask_all(self,event):
        print('\nRepeating the same mask for all projections')
        print('Please wait...')
        #nproj = self.X1.shape[0]
        mask = self.ROI_draw.getMask(self.img_mask)
        self.mask |= np.array([mask for _ in range(self.slices)])
        print('Done')
        self.update()
    
    # remove the current selected area from the mask
    def remove_mask(self,event):
        print('\nRemoving mask')     
        #self.mask[self.mask_ind,:,:] &= ~self.ROI_draw.getMask(self.img_mask)
        self.mask[self.ind,:,:] &= ~self.ROI_draw.getMask(self.img_mask)
        self.update()
        
    # remove all the masks
    def remove_all_mask(self,event):
        print('\nRemoving all mask')    
        print('Please wait...')
        mask = self.ROI_draw.getMask(self.img_mask)
        #self.mask[self.mask_ind,:,:] &= ~self.ROI_draw.getMask(self.img_mask)
        self.mask &= ~np.array([mask for _ in range(self.slices)])
        print('Done')
        self.update()
    
    # Apply the linear phase correction using current mask    
    def apply_mask(self,event):
        print('\nApply the linear phase correction using current mask')
        imgin = np.exp(1j*self.X1[self.ind,:,:]).copy()
        mask = self.mask[self.ind,:,:].copy()
        #~ self.X1[self.ind,:,:]=np.angle(remove_linearphase(imgin,mask,100)).copy()
        self.X1[self.ind,:,:]=np.angle(remove_linearphase(imgin,mask)).copy()
        self.X2[self.ind,:]=self.X1[self.ind,np.int(self.X1.shape[1]/2.),:].copy()
        self.update()
    
    # Apply the linear phase correction using current mask to all projections    
    def apply_all_masks(self,event):
        print('\nApply the linear phase correction using current mask to all projections')
        for ii in range(self.slices):
            self.ind=ii
            print('Projection {} out of {}'.format(ii+1,self.slices),end='\r')
            imgin = np.exp(1j*self.X1[ii,:,:]).copy()
            mask = self.mask[ii,:,:].copy()
            #~ self.X1[ii,:,:]=np.angle(remove_linearphase(imgin,mask,100)).copy()
            self.X1[ii,:,:]=np.angle(remove_linearphase(imgin,mask)).copy()
            self.X2[ii,:]=self.X1[ii,np.int(self.X1.shape[1]/2.),:].copy()
            self.update()
        print('\r')
        print('Done')
    
    # Remove linear phase ramp        
    def remove_ramp(self,event):
        print('\nRemove linear ramp')
        self.X1[self.ind,:,:] = np.angle(rmphaseramp(np.exp(1j*self.X1[self.ind,:,:]),weight=None,return_phaseramp=False))
        #mask = self.mask[self.ind,:,:]*self.X1[self.ind,:,:]
        #self.X1[self.ind,:,:] -= mask.astype(np.float).mean()
        self.X2[self.ind,:]=self.X1[self.ind,np.int(self.X1.shape[1]/2.),:].copy()
        self.update()

    # Remove linear phase ramp of all
    def remove_rampall(self,event):
        print('\nRemove linear phase ramp of all projections')
        for ii in range(self.slices):
            self.ind=ii
            print('Projection {} out of {}'.format(ii+1,self.slices),end='\r')
            self.X1[self.ind,:,:] = np.angle(rmphaseramp(np.exp(1j*self.X1[self.ind,:,:]),weight=None,return_phaseramp=False))
            #mask = self.mask[self.ind,:,:]*self.X1[self.ind,:,:]
            #self.X1[self.ind,:,:] -= mask.astype(np.float).mean()
            self.X2[self.ind,:]=self.X1[self.ind,np.int(self.X1.shape[1]/2.),:].copy()
            self.update()
        print('\r')
        print('Done')

    # Unwrap phase       
    def unwrapping_phase(self,event):
        print('\nUnwrapping phase')
        self.X1[self.ind,:,:] = unwrap_phase(self.X1[self.ind,:,:])
        if np.any(self.mask[self.ind,:,:]==True):
            vals = self.X1[self.ind,:,:][np.where(self.mask[self.ind,:,:]==True)].mean()
            self.X1[self.ind,:,:] -= 2*np.pi*np.round(vals/(2*np.pi))
        self.X2[self.ind,:]=self.X1[self.ind,np.int(self.X1.shape[1]/2.),:].copy()
        self.update()

    # Unwrap phase of all
    def unwrapping_all(self,event):
        print('\nRemove linear phase ramp of all projections')
        for ii in range(self.slices):
            self.ind=ii
            print('Projection {} out of {}'.format(ii+1,self.slices),end='\r')
            self.X1[self.ind,:,:] = unwrap_phase(self.X1[self.ind,:,:])
            if np.any(self.mask[self.ind,:,:]==True):
                vals = self.X1[self.ind,:,:][np.where(self.mask[self.ind,:,:]==True)].mean()
                self.X1[self.ind,:,:] -= 2*np.pi*np.round(vals/(2*np.pi))
            self.X2[self.ind,:]=self.X1[self.ind,np.int(self.X1.shape[1]/2.),:].copy()
            self.update()
        print('\r')
        print('Done')

    # Load masks from file        
    def load_masks(self,event):
        print('\nLoad masks from file')
        Lm = LoadData(**self.params)
        self.mask = Lm.load_masks('linear_phase_ramp_masks.h5')
        self.update()
    
    # Save mask to file    
    def save_masks(self,event):
        print('\nSave masks to file')
        Sm = SaveData(**self.params)
        Sm.save_masks('linear_phase_ramp_masks.h5',self.mask)
    
    # move projection number down using button Prev    
    def down(self, event):
        self.ind = np.clip(self.ind - 1, 0, self.slices - 1)
        print("Projection {}".format(self.ind+1))
        self.update()
    
    # move projection number up using button Next
    def up(self, event):
        self.ind = np.clip(self.ind + 1, 0, self.slices - 1)
        print("Projection {}".format(self.ind+1))
        self.update()

    # textbox submit
    def submit(self,text):
        self.ind = np.clip(eval(text)-1,0,self.slices - 1)
        print("Projection {}".format(self.ind+1))
        self.update()

    #~ def updateval(self,text):
        #~ self.update()
        #return str(self.ind)

    # Plot one project after the other (play)
    def play(self, event):
        for ii in range(self.ind, self.slices):
            self.ind = ii #np.clip(self.ind + ii, 0, self.slices - ii)
            print("Projection {}".format(self.ind+1))
            self.update()
    
    # Update the plot
    def update(self):
        self.im1.set_data(self.X1[self.ind, :, :]+self.mask[self.ind,:,:])
        self.im1.set_clim(self.vmin,self.vmax)
        #self.vmin,self.vmax = self.im1.get_clim()
        self.im2.set_ydata(self.X2[self.ind, :])
        self.im2.axes.set_ylim([self.pmin,self.pmax])
        self.ax1.set_ylabel('Projection {}'.format(self.ind+1))
        self.ax2.set_ylabel('Projection {}'.format(self.ind+1))
        self.ax1.axes.figure.canvas.draw()
        self.ax2.axes.figure.canvas.draw()
        #self.im1.axes.figure.canvas.draw()
        #self.im2.axes.figure.canvas.draw()

    # Close the figure
    def onclose(self, event):
        print('\nImage closed. Press Enter to exit and save projections')
        plt.close(event.canvas.figure)
