import glob
import os, re
import pprint

foldername = '/data/id16a/inhouse2/staff/js/ihch1219/id16a/catalyst_prist_40nm/catalyst_prist_40nm_analysis/recons/catalyst_prist_40nm_8px_nfptomo_subtomo001_0000/catalyst_prist_40nm_8px_nfptomo_subtomo001_0000_ML.ptyr'

#foldername = '/data/id16a/inhouse4/visitor/ch5449/id16a/reference0h_second/reference0h_second_analysis/recons/reference0h_second_50nm_lowerdose_nfptomo_subtomo001_0000/reference0h_second_50nm_lowerdose_nfptomo_subtomo001_0000_ML.ptyr'
#foldername = 'recons/gp2_NaCl_dif_pitch_nfp_tomo16nm_subtomo001_0000'
#foldername = '../analysis_pynx3/H2int_15000h_inlet_25nm_nfptomo_subtomo001_0000.cxi'
file_wcard = re.sub('\d{3}_\d{4}','*',foldername)
reconlist = sorted(glob.glob(file_wcard))
folderlist = [re.sub('_ML','',os.path.splitext(os.path.basename(ii))[0]) for ii in reconlist]
#folderlist = [os.path.basename(ii) for ii in reconlist]
#folderlist = [os.path.splitext(os.path.basename(ii))[0] for ii in reconlist]

ptydfile_wcard = re.sub('\d{3}_\d{4}_ML.ptyr','*.ptyd',os.path.basename(foldername))
#ptydfile_wcard = re.sub('\d{3}_\d{4}$','*.ptyd',os.path.basename(foldername))
#ptydfile_wcard = '../'+re.sub('\d{3}_\d{4}.cxi','*.ptyd',os.path.basename(foldername))
ptydfiles = sorted(glob.glob(ptydfile_wcard))
ptydlist = [os.path.splitext(os.path.basename(ii))[0] for ii in ptydfiles]

diff_list = sorted([ii for ii in (set(folderlist) ^ set(ptydlist))])

#~ print(diff_list)
print('There are {} missing reconstructions'.format(len(diff_list)))
pprint.pprint(diff_list)

