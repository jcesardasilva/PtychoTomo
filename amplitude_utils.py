#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Wed Nov  4 16:22:54 2015

@author: jdasilva
"""
from __future__ import division, print_function,unicode_literals

# third party packages
import matplotlib.pyplot as plt
import numpy as np
from scipy.fftpack import fftfreq

# local packages
from io_utils import save_or_load_masks
from roipoly import roipoly

__all__=[ u'IndexTracker']
    
def remove_air(image,mask):
    """
    Correcting amplitude factor using the mask from the phase ramp removal
    considering only pixels where mask is
    unity, arrays have center on center of array
    Inputs:
        image  = Amplitude Image
        mask   = Binary array with ones where the linear phase should be
                  computed from
    @author: Julio Cesar da Silva (e-mail:jdasilva@esrf.fr)

    """
    norm_val = np.sum(mask*image)/mask.sum()
    print("Normalization value: {}".format(norm_val))
    return image/norm_val

class IndexTracker(object):
    def __init__(self, ax1, ax2, X1, pathfilename):
        #self.fig = plt.figure(4)
        self.ax1 = ax1
        self.ax2 = ax2
        ax1.set_title('Use scroll wheel or \n left/right arrows to navigate images')

        self.X1 = X1.copy()
        if np.iscomplexobj(self.X1):
            raise ValueError('The array is complex and must be only amplitude')
            
        #self.X2 = X2
        self.X2=self.X1[:,np.int(X1.shape[1]/2.),:].copy()
        self.mask = np.zeros_like(X1,dtype=np.bool8)
        self.slices,rows, cols = X1.shape
        self.ind = 0
        self.done = []
        #self.mask_ind = []
        self.pathfilename = pathfilename # to be fixed

        self.im1 = self.ax1.imshow(self.X1[self.ind, :, :],cmap='gray', vmin = 0)#, vmax = np.pi/2)
        self.vmin,self.vmax = self.im1.get_clim() # get colormap limits
        self.ax1.axis('tight')
        self.im2, = self.ax2.plot(self.X2[self.ind, :])
        self.ax2.plot([0,self.X1.shape[2]],[0,0])
        self.pmin,self.pmax = self.ax2.get_ylim() # get plot limits
        #ax2.axis('tight')
        #self.ax2.set_ylim([-np.pi,np.pi])
        self.ax2.set_xlim([0,cols])
        self.update()
        print('Done. When finished, close figure and press <Enter> to exit')
    # set the vmin and vmax on colormap
    def cmvmin(self,val):
        if eval(val)>= self.vmax:
            print('vmin is equal or larger than vmax. Choose smaller value')
        else:
            self.vmin = eval(val) #np.clip(eval(val), 0, self.vmax - 1)
            self.pmin = self.vmin
            self.update()

    def cmvmax(self,val):
        if eval(val)<= self.vmin:
            print('vmax is equal or smaller than vmin. Choose larger value')
        else:
            self.vmax = eval(val)
            self.pmax = self.vmax
            self.update()
        
    # move projection number up/down using the mouse scroll wheel
    def onscroll(self, event):
        if event.button == 'up':
            if self.ind==self.slices:#X1.shape[0]:
                print('You reached the maximum number of projections')
            else:
                self.ind = np.clip(self.ind + 1, 0, self.slices - 1)
                print("{} {} - projection {}".format(event.button, event.step,self.ind+1))
        else:
            if self.ind==0:
                print('You reached the first projection')
            else:
                self.ind = np.clip(self.ind - 1, 0, self.slices - 1)
                print("{} {} - projection {}".format(event.button, event.step,self.ind+1))
        self.update()
    # move projection number up/down using right/left arrows in the keyboard
    def key_event(self, event):
        if event.key == 'right':
            if self.ind==self.X1.shape[0]:
                print('You reached the maximum number of projections')
            else:
                self.ind = np.clip(self.ind + 1, 0, self.slices - 1)
                print("{} - projection {}".format(event.key,self.ind+1))
        elif event.key == 'left':
            if self.ind==0:
                print('You reached the first projection')
            else:
                self.ind = np.clip(self.ind - 1, 0, self.slices - 1)
                print("{} - projection {}".format(event.key,self.ind+1))
        else:
            return
        self.update()
    
    # Draw the mask using roipoly.py    
    def draw_mask(self,event):
        #if self.mask_shape == 'p':
        print('\nDrawing the poly')
        self.img_mask = self.X1[self.ind, :, :]+self.mask[self.ind,:,:]
        fig_mask = plt.figure()
        ax_mask = fig_mask.add_subplot(111)
        ax_mask.imshow(self.img_mask,cmap='bone')
        self.ROI_draw = roipoly(ax=ax_mask)
        #self.update()
        #self.ROI_draw = roipoly(ax=self.ax1)
        
        #self.mask_ind.append(self.ind)
        #self.mask_ind = self.ind.copy()
    
    # add the mask to the plot    
    def add_mask(self,event):
        print('\nAdding mask')
        #self.mask[self.mask_ind,:,:] |= self.ROI_draw.getMask(self.img_mask)
        self.mask[self.ind,:,:] |= self.ROI_draw.getMask(self.img_mask)
        #if len(self.mask_ind)==1:
        #    self.mask[self.mask_ind[0],:,:] |= self.ROI_draw.getMask(self.img_mask)
        #else:
        #    for ii in self.mask_ind:
        #       self.mask[ii,:,:] |= self.ROI_draw.getMask(self.img_mask) 
        self.ROI_draw.displayROI()
        self.update()
    
    # use the same mask for all projections        
    def mask_all(self,event):
        print('\nRepeating the same mask for all projections')
        print('Please wait...')
        #nproj = self.X1.shape[0]
        mask = self.ROI_draw.getMask(self.img_mask)
        self.mask |= np.array([mask for _ in range(self.slices)])
        print('Done')
        self.update()
    
    # remove the current selected area from the mask
    def remove_mask(self,event):
        print('\nRemoving mask')     
        #self.mask[self.mask_ind,:,:] &= ~self.ROI_draw.getMask(self.img_mask)
        self.mask[self.ind,:,:] &= ~self.ROI_draw.getMask(self.img_mask)
        self.update()
        
    # remove all the masks
    def remove_all_mask(self,event):
        print('\nRemoving all mask')    
        print('Please wait...')
        mask = self.ROI_draw.getMask(self.img_mask)
        #self.mask[self.mask_ind,:,:] &= ~self.ROI_draw.getMask(self.img_mask)
        self.mask &= ~np.array([mask for _ in range(self.slices)])
        print('Done')
        self.update()
    
    # Apply the linear phase correction using current mask    
    def apply_mask(self,event):
        print('\nApply the linear phase correction using current mask')
        if self.ind in self.done:
            print('Projection {} was already corrected'.format(self.ind+1))
        else:
            imgin = self.X1[self.ind,:,:].copy() #np.exp(1j*self.X1[self.ind,:,:]).copy()
            mask = self.mask[self.ind,:,:].copy()
            self.X1[self.ind,:,:]=np.log(remove_air(imgin,mask)) # remove air and apply log
            self.X2[self.ind,:]=self.X1[self.ind,np.int(self.X1.shape[1]/2.),:].copy()
            self.done.append(self.ind)
            self.vmin=-0.5
            self.vmax=0.1
        self.update()
    
    # Apply the linear phase correction using current mask to all projections    
    def apply_all_masks(self,event):
        print('\nApply the linear phase correction using current mask to all projections')
        for ii in range(self.slices):
            self.ind=ii
            print('Projection {}'.format(ii+1))
            if self.ind in self.done:
                print('Projection {} was already corrected'.format(self.ind+1))
            else:
                imgin = self.X1[ii,:,:].copy() #np.exp(1j*self.X1[ii,:,:]).copy()
                mask = self.mask[ii,:,:].copy()
                self.X1[ii,:,:]=np.log(remove_air(imgin,mask)) # remove air and apply log
                self.X2[ii,:]=self.X1[ii,np.int(self.X1.shape[1]/2.),:].copy()
                self.done.append(self.ind)
            self.update()
        print('Done')
    
    # Load masks from file        
    def load_masks(self,event):
        self.mask = save_or_load_masks(pathfilename=self.pathfilename,h5name='linear_phase_ramp_masks.h5')
        print('\nLoad masks from file')
        self.update()
    
    # Save mask to file    
    def save_masks(self,event):
        self.mask = save_or_load_masks(self.mask,pathfilename=self.pathfilename,h5name='linear_phase_ramp_masks.h5')
        print('\nSave masks to file')
    
    # move projection number down using button Prev    
    def down(self, event):
        self.ind = np.clip(self.ind - 1, 0, self.slices - 1)
        print("Projection {}".format(self.ind+1))
        self.update()
    
    # move projection number up using button Next
    def up(self, event):
        self.ind = np.clip(self.ind + 1, 0, self.slices - 1)
        print("Projection {}".format(self.ind+1))
        self.update()

    # textbox submit
    def submit(self,text):
        self.ind = np.clip(eval(text)-1,0,self.slices - 1)
        print("Projection {}".format(self.ind+1))
        self.update()

    #~ def updateval(self,text):
        #~ self.update()
        #return str(self.ind)

    # Plot one project after the other (play)
    def play(self, event):
        for ii in xrange(self.ind, self.slices):
            self.ind = ii #np.clip(self.ind + ii, 0, self.slices - ii)
            print("Projection {}".format(self.ind+1))
            self.update()
    
    # Update the plot
    def update(self):
        self.im1.set_data(self.X1[self.ind, :, :]+self.mask[self.ind,:,:])
        self.im1.set_clim(self.vmin,self.vmax)
        #self.vmin,self.vmax = self.im1.get_clim()
        self.im2.set_ydata(self.X2[self.ind, :])
        self.im2.axes.set_ylim([self.pmin,self.pmax])
        self.ax1.set_ylabel('Projection {}'.format(self.ind+1))
        self.ax2.set_ylabel('Projection {}'.format(self.ind+1))
        self.ax1.axes.figure.canvas.draw()
        self.ax2.axes.figure.canvas.draw()
        #self.im1.axes.figure.canvas.draw()
        #self.im2.axes.figure.canvas.draw()

    # Close the figure
    def onclose(self, event):
        print('\nImage closed. Press Enter to exit and save projections')
        plt.close(event.canvas.figure)
